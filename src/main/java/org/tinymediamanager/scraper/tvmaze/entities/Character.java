package org.tinymediamanager.scraper.tvmaze.entities;

public class Character {
  public int       id;
  public String    url;
  public String    name;
  public ImageUrls image;
}
