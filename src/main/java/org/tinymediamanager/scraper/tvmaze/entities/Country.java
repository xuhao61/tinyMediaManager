package org.tinymediamanager.scraper.tvmaze.entities;

public class Country {
  public String name;
  public String code;
  public String timezone;
}
