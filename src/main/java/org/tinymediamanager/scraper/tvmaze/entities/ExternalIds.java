package org.tinymediamanager.scraper.tvmaze.entities;

public class ExternalIds {
  public int    tvrage;
  public int    thetvdb;
  public String imdb;
}
