package org.tinymediamanager.scraper.tvmaze.entities;

public class ImageUrls {
  public String medium;
  public String original;
}
