package org.tinymediamanager.scraper.tvmaze.entities;

public class Network {
  public int     id;
  public String  name;
  public Country country;
  public String  officialSite;
}
