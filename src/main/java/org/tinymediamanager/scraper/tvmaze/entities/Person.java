package org.tinymediamanager.scraper.tvmaze.entities;

public class Person {
  public int       id;
  public String    url;
  public String    name;
  public Country   country;
  public String    birthday;
  public String    deathday;
  public String    gender;
  public ImageUrls image;
}
