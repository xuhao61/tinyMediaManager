package org.tinymediamanager.scraper.tvmaze.entities;

/**
 * Same as Network
 *
 */
public class WebChannel {
  public int     id;
  public String  name;
  public Country country;
  public String  officialSite;
}
